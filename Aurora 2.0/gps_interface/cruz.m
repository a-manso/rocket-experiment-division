function cruz(k, f, A)
%define as coordenadas da cruz
p1(1) = f(1) - k;
p1(2) = f(2) + k;
p2 = f + k;
p3 = f - k;
p4(1) = f(1) + k;
p4(2) = f(2) - k;

if(A==0)
    plot([p1(1),p4(1)],[p1(2),p4(2)],'Color','r','LineWidth',2)
    plot([p3(1),p2(1)],[p3(2),p2(2)],'Color','r','LineWidth',2)
else
    line(A, [p1(1),p4(1)],[p1(2),p4(2)],'Color','r','LineWidth',2);
    line(A, [p3(1),p2(1)],[p3(2),p2(2)],'Color','r','LineWidth',2);
end

end

