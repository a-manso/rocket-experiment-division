#include <SoftwareSerial.h>

SoftwareSerial loraSerial(2, 3); // TX, RX
const int LORA_AUX = 8;

void setup() {
  Serial.begin(9600);
  loraSerial.begin(9600);
  pinMode(LORA_AUX, INPUT);
  Serial.println("acordei BELLE");
}

void loop()
{
  if (loraSerial.available() > 1)
  {
    String input = loraSerial.readString();
    Serial.print("RECEBI DA AURORA: ");
    Serial.println(input);
  }

  if (Serial.available() > 1)
  {
    String ejet = Serial.readString();
    Serial.print("MANDASTE-ME ENVIAR: ");
    Serial.println(ejet);

    String s = "Nada";
    bool vamos_enviar = 0;
    
    if (ejet.indexOf('E') != -1)
    {
      Serial.println("ENVIEI EMERGENCY");
      s = "RED_SOS";
      vamos_enviar = 1;
    }
    
    if (ejet.indexOf('T') != -1)
    {
      Serial.println("ENVIEI TIMER");
      s = "RED_TIMER";
      vamos_enviar = 1;
    } 
    
    if (ejet.indexOf('A') != -1)
    {
      Serial.println("ENVIEI ACELERAÇÃO");
      s = "RED_ACCEL";
      vamos_enviar = 1;
    }
    
    if (vamos_enviar)
    {
      while(1-digitalRead(LORA_AUX)){
        Serial.println("vou esperar pela lora");
      }
      //se esta pronto para enviar coisas
      loraSerial.println(s);
      Serial.println("ENVIEI EFETIVAMENTE");
      delay(250);
    } else {
      //se nao vai enviar
      Serial.println("NAO vou enviar");
    }
  }
}
