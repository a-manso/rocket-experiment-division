from network import LoRa
import socket
import machine
import time
import pycom
from LIS2HH12 import LIS2HH12
from L76GNSS import L76GNSS
from pytrack import Pytrack

pycom.heartbeat(False)

pycom.rgbled(0xFF0000) # Red

lora = LoRa(mode=LoRa.LORA, region=LoRa.EU868)

# create a raw LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

acc = LIS2HH12()

py = Pytrack()
l76 = L76GNSS(py, timeout=30)

print('Rocket Awakes!')

while True:

    # get any data received...
    s.setblocking(False)
    data = s.recv(64)
    if len(data) is not 0:
        print(data)

        pycom.rgbled(0x00FF00)  # Green

        time.sleep(1)

        pycom.rgbled(0x0000FF)  # Blue
    else:
        pitch = acc.pitch()
        roll = acc.roll()
        coord = l76.coordinates()

        print("{},{} Location:{}".format(pitch, roll, coord))

        pycom.rgbled(0x00FF00)  # Green

        # send some data
        s.setblocking(True)
        s.send("{},{} Location:{}".format(pitch, roll, coord))

        pycom.rgbled(0x0000FF)  # Blue

        # time.sleep_ms(100)
