from network import LoRa
import socket
import machine
import time
import pycom


pycom.heartbeat(False)

pycom.rgbled(0xFF0000) # Red

lora = LoRa(mode=LoRa.LORA, region=LoRa.EU868)

s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

print('Base Station Awakes!')

while True:
    # send some data
    # s.setblocking(True)
    # s.send('Hello')

    # get any data received...
    # s.setblocking(False)
    data = s.recv(128)

    if len(data) is not 0:
        print(data)

        pycom.rgbled(0x00FF00)  # Green

        time.sleep_ms(50)

        pycom.rgbled(0x0000FF)  # Blue
